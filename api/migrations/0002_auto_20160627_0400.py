# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date
import pprint

import json
from django.db import migrations, models


def load_data(apps, schema_editor):
    # We can't import the model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    HexGrid = apps.get_model('api', 'HexGrid')

    # TODO: use json.load()
    data_file = 'grids.json'
    f = open(data_file)
    grids = [HexGrid(**json.loads(line))
               for line in f
               ]
    HexGrid.objects.bulk_create(grids)


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_data),
    ]
