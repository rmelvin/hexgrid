import re
import uuid

from django.core.validators import _lazy_re_compile, RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


def hex_list_validator(sep=',', message=None, code='invalid'):
    regexp = _lazy_re_compile('^[0-9a-f]+(?:%s[0-9a-f]+)*\Z' % re.escape(sep),
                              re.IGNORECASE)
    return RegexValidator(regexp, message=message, code=code)


validate_comma_separated_hex_list = hex_list_validator(
    message=_('Enter only hex values separated by commas.'),
)


class HexGrid(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    # TODO: formalize max length in terms of grid size
    data = models.CharField(max_length=4096, blank=True, unique=True,
                            validators=[validate_comma_separated_hex_list])
    rows = models.PositiveIntegerField(blank=False, default=1)
    cols = models.PositiveIntegerField(blank=False, default=1)

    class Meta:
        verbose_name = _('grid')
        verbose_name_plural = _('grids')

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return 'HexGrid(id={})'.format(repr(self.id))
