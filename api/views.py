from operator import itemgetter

from rest_framework import permissions, views, viewsets, renderers
from rest_framework.decorators import detail_route
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from api.models import HexGrid
from api.serializers import HexGridSerializer


def _load_grid_from_seq(data, rows, cols):
    """construct two-dimensional array from linear sequence

    note:
    This func does not validate data.
    This func should only be called from a view, and that view should
    reference an already validated `HexGrid` model.

    :param data: linear data sequence of hexadecimal chars
    :type data: list
    :param rows: number of grid rows
    :type rows: int
    :param cols: number of grid columns
    :type cols: int
    :return grid: two-dimensional structure
    :rtype grid: list of lists
    """
    # paranoia check in the event caller is not a view
    if rows * cols != len(data):
        raise Exception('data, rows, and cols mismatch')

    row_range = range(rows)
    grid = [[] for i in row_range]
    for y in range(rows):
        for x in range(cols):
            grid[y].append(data.pop(0))

    return grid


def find_next(grid, currx, curry, endx, endy):
    """Find the next "least cost" move on the grid

    :param grid: two-dimensional grid
    :type grid: list of lists
    :param currx: current grid x position
    :type currx: int
    :param curry: current grid y position
    :type curry: int
    :param endx: end/target grid x position
    :type endx: int
    :param endy: end/target grid y position
    :type endy: int
    :return pick_x, pick_y: the location picked by algo
    :rtype pick_x, pick_y: int, int
    """
    if not grid:
        return (None, None)

    curr = grid[curry][currx]
    dir = detect_direction(currx, curry, endx, endy)
    if not dir:
        return (None, None)

    choices = {}
    # Lets program the choices based on direction and current position
    if currx == endx:
        if dir.startswith('s'):
            choices = {'down': {'x': currx, 'y': curry + 1, }, }
        elif dir.startswith('n'):
            choices = {'up': {'x': currx, 'y': curry - 1, }, }
    elif curry == endy:
        if dir.endswith('e'):
            choices = {'right': {'x': currx + 1, 'y': curry, }, }
        elif dir.endswith('w'):
            choices = {'left': {'x': currx - 1, 'y': curry, }, }
    # TODO: use enumerated directions
    else:
        if dir == 'se':
            choices = {'right': {'x': currx + 1, 'y': curry, },
                       'down': {'x': currx, 'y': curry + 1, },
                       }
        elif dir == 'ne':
            choices = {'right': {'x': currx + 1, 'y': curry, },
                       'up': {'x': currx, 'y': curry - 1, },
                       }
        elif dir == 'sw':
            choices = {'left': {'x': currx - 1, 'y': curry, },
                       'down': {'x': currx, 'y': curry + 1, },
                       }
        elif dir == 'nw':
            choices = {'left': {'x': currx - 1, 'y': curry, },
                       'up': {'x': currx, 'y': curry - 1, },
                       }

    # TODO: hmmm...diff might not be needed. A simple comparison of the
    # choices (and determining smallest one) should suffice.
    samples = [(compute_hex_diff(grid[position['y']][position['x']], curr),
                {'x': position['x'], 'y': position['y'], }
                )
               for (direction, position) in choices.items()
               ]
    if not samples:
        return (None, None)

    pick = min(samples, key=itemgetter(0))
    return (pick[1].get('x'), pick[1].get('y'))


def detect_direction(startx, starty, endx, endy):
    """Detect direction from start to end

    :param startx: starting x position
    :type startx: int
    :param starty: starting y position
    :type starty: int
    :param endx: ending x position
    :type endx: int
    :param endy: ending y position
    :type endy: int
    :return dir: direction being travelled
    :rtype dir: str
    """
    # TODO: enumerate directions
    # east
    if endx > startx:
        if endy > starty:
            direction = 'se'
        elif endy < starty:
            direction = 'ne'
        else:
            direction = 'e'
    # west
    elif endx < startx:
        if endy > starty:
            direction = 'sw'
        elif endy < starty:
            direction = 'nw'
        else:
            direction = 'w'
    # north or south
    else:
        if endy > starty:
            direction = 's'
        elif endy < starty:
            direction = 'n'
        # same -> no direction
        else:
            direction = None

    return direction


def compute_hex_diff(hex1, hex2):
    """Determine difference between two hexadecimal strings

    :param hex1: hexadecimal operand 1
    :type hex1: str
    :param hex2: hexadecimal operand 2
    :type hex2: str
    :return diff: difference
    :rtype diff: int
    """
    return int(hex1, base=16) - int(hex2, base=16)


class WalkGridView(views.APIView):
    """Handle walking a grid view. Returns list of coordinates

    Request Params:
    :param grid_id: (required) grid uid to walk
    :type grid_id: str
    :param startx: (optional) starting x position (default 0)
    :param starty: (optional) starting y position (default 0)
    :param endx: (optional) ending x position (default NUM_COLS-1)
    :param endy: (optional) ending y position (default NUM_ROWS-1)

    e.g. walk southeast (default)
    GET /walk/?grid_id=<grid_id>

    e.g. walk southwest
    GET /walk/?grid_id=<grid_id>&startx=2&starty=0&endx=0&endy=2

    e.g. walk northeast
    GET /walk/?grid_id=<grid_id>&startx=0&starty=2&endx=2&endy=0

    e.g. walk northwest
    GET /walk/?grid_id=<grid_id>&startx=2&starty=2&endx=0&endy=0

    Response:
    [ [x1,y1], .., [xN,yM] ]
    """
    def get(self, request, format=None):
        grid_id = request.query_params.get('grid_id')
        if not grid_id:
            raise ParseError('must specify grid id')

        grid_instance = HexGrid.objects.get(pk=grid_id)
        NUM_ROWS = grid_instance.rows
        NUM_COLS = grid_instance.cols
        # TODO: handle invalid input
        startx = int(request.query_params.get('startx', 0))
        starty = int(request.query_params.get('starty', 0))
        endx = int(request.query_params.get('endx', NUM_COLS - 1))
        endy = int(request.query_params.get('endy', NUM_ROWS - 1))
        grid_data = _load_grid_from_seq(grid_instance.data.strip().split(','),
                                        NUM_ROWS, NUM_COLS)
        points = [(startx, starty)]
        next_x, next_y = find_next(grid_data, startx, starty, endx=endx,
                                   endy=endy)
        while (next_x, next_y) != (endx, endy):
            points.append((next_x, next_y))
            next_x, next_y = find_next(grid_data, next_x, next_y, endx=endx,
                                       endy=endy)

        points.append((endx, endy))
        return Response(points)


class HexGridViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally, we also provide an extra `highlight` action.
    """
    queryset = HexGrid.objects.all()
    serializer_class = HexGridSerializer
    permission_classes = (permissions.IsAuthenticated, )

    """
    This decorator can be used to add any custom endpoints that don't fit
    into the standard create/update/delete style
    """
    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        product = self.get_object()
        return Response(product.highlighted)

    def get_queryset(self):
        """Limit the `HexGrid` listing depending on who you are logged on as

        As a superuser you see all HexGrids.
        """
        if self.request.user.is_superuser:
            return HexGrid.objects.all()
        else:
            return HexGrid.objects.none()

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        # Create a db entry for our new resource
        serializer.save()


class LoadView(views.APIView):
    """
    Ingest additional grids into db
    This can also be accomplished with POST /grids/

    Request Params:
    :param data: grid data info (data, rows, cols)
    """
    def post(self, request, format=None):
        data = request.data
        if not data:
            # TODO: return 400
            return

        HexGrid.objects.create(**data)
        return Response('grid added'.format())
