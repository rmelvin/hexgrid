from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from api.models import HexGrid


class HexGridSerializer(serializers.ModelSerializer):
    """Serializer assuming the default REST framework handling capabilities"""
    class Meta:
        model = HexGrid
        fields = ('id', 'data', 'rows', 'cols')

    def create(self, validated_data):
        data = validated_data.get('data').strip().split(',')
        rows = validated_data.get('rows', 1)
        cols = validated_data.get('cols', 1)
        if rows * cols != len(data):
            raise serializers.ValidationError(
                {'data': _('rows and cols do not match data length')})

        return HexGrid.objects.create(**validated_data)
