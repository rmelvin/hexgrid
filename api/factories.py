import factory

from .models import HexGrid


class HexGridFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = HexGrid
