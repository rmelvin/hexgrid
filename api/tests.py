from django.test import TestCase

from api.views import _load_grid_from_seq, find_next, detect_direction


class LoadGridTestCase(TestCase):
    def setUp(self):
        self.onedsequence = ['00F', '45D', '321', 'FFF']
        self.twodsequence = [['00F', '45D'], ['321', 'FFF']]

    def test_bad_dimensions(self):
        with self.assertRaises(Exception):
            _load_grid_from_seq(self.onedsequence, 1, 2)

    def test_load_good_grid_data(self):
        grid = _load_grid_from_seq(self.onedsequence, 2, 2)
        self.assertListEqual(self.twodsequence, grid)


class FindNextTestCase(TestCase):
    # TODO: test out of bounds start index

    """
    00F 45D 001
    321 FFF 555
    66A BC2 777
    """
    def setUp(self):
        self.twodsequence = [['00F', '45D', '001'],
                             ['321', 'FFF', '555'],
                             ['66A', 'BC2', '777'],
                             ]
        self.NUM_COLS = 3
        self.NUM_ROWS = 3

    def test_no_grid(self):
        x, y = find_next(None, 0, 0, 0, 0)
        [self.assertIsNone(i) for i in (x, y)]

    def test_se_next_position_right(self):
        x, y = find_next(self.twodsequence, 1, 0, self.NUM_COLS-1,
                         self.NUM_ROWS-1)
        self.assertEquals((2, 0), (x, y))

    def test_se_next_position_must_right(self):
        x, y = find_next(self.twodsequence, 0, self.NUM_ROWS-1,
                         self.NUM_COLS-1, self.NUM_ROWS-1)
        self.assertEquals((1, self.NUM_ROWS-1), (x, y))

    def test_se_next_position_down(self):
        x, y = find_next(self.twodsequence, 0, 0, self.NUM_COLS-1,
                         self.NUM_ROWS-1)
        self.assertEquals((0, 1), (x, y))

    def test_se_next_position_must_down(self):
        x, y = find_next(self.twodsequence, self.NUM_COLS-1, 0,
                         self.NUM_COLS-1, self.NUM_ROWS-1)
        self.assertEquals((self.NUM_COLS-1, 1), (x, y))

    def test_se_at_end(self):
        x, y = find_next(self.twodsequence, self.NUM_COLS-1, self.NUM_ROWS-1,
                         self.NUM_COLS-1, self.NUM_ROWS-1)
        [self.assertIsNone(i) for i in (x, y)]

    def test_ne_next_position_right(self):
        x, y = find_next(self.twodsequence, 1, 2, self.NUM_COLS-1, 0)
        self.assertEquals((2, 2), (x, y))

    def test_ne_next_position_must_right(self):
        x, y = find_next(self.twodsequence, 0, 0, self.NUM_COLS-1, 0)
        self.assertEquals((1, 0), (x, y))

    def test_ne_next_position_up(self):
        x, y = find_next(self.twodsequence, 0, 2, self.NUM_COLS-1, 0)
        self.assertEquals((0, 1), (x, y))

    def test_ne_next_position_must_up(self):
        x, y = find_next(self.twodsequence, self.NUM_COLS-1, 2,
                         self.NUM_COLS-1, 0)
        self.assertEquals((self.NUM_COLS-1, 1), (x, y))

    def test_ne_at_end(self):
        x, y = find_next(self.twodsequence, self.NUM_COLS-1, 0,
                         self.NUM_COLS-1, 0)
        [self.assertIsNone(i) for i in (x, y)]

    def test_nw_next_position_left(self):
        x, y = find_next(self.twodsequence, 1, 1, 0, 0)
        self.assertEquals((0, 1), (x, y))

    def test_nw_next_position_must_left(self):
        x, y = find_next(self.twodsequence, 2, 0, 0, 0)
        self.assertEquals((1, 0), (x, y))

    def test_nw_next_position_up(self):
        x, y = find_next(self.twodsequence, 2, 2, 0, 0)
        self.assertEquals((2, 1), (x, y))

    def test_nw_next_position_must_up(self):
        x, y = find_next(self.twodsequence, 0, 2, 0, 0)
        self.assertEquals((0, 1), (x, y))

    def test_nw_at_end(self):
        x, y = find_next(self.twodsequence, 0, 0, 0, 0)
        [self.assertIsNone(i) for i in (x, y)]

    def test_sw_next_position_left(self):
        x, y = find_next(self.twodsequence, 2, 0, 0, self.NUM_ROWS-1)
        self.assertEquals((1, 0), (x, y))

    def test_sw_next_position_must_left(self):
        x, y = find_next(self.twodsequence, 2, self.NUM_ROWS-1, 0,
                         self.NUM_ROWS-1)
        self.assertEquals((1, self.NUM_ROWS-1), (x, y))

    def test_sw_next_position_down(self):
        x, y = find_next(self.twodsequence, 2, 1, 0, self.NUM_ROWS-1)
        self.assertEquals((2, 2), (x, y))

    def test_sw_next_position_must_down(self):
        x, y = find_next(self.twodsequence, 0, 0, 0, self.NUM_ROWS-1)
        self.assertEquals((0, 1), (x, y))

    def test_sw_at_end(self):
        x, y = find_next(self.twodsequence, 0, self.NUM_ROWS-1, 0,
                         self.NUM_ROWS-1)
        [self.assertIsNone(i) for i in (x, y)]

    def test_1x1(self):
        x, y = find_next(['fff'], 0, 0, 0, 0)
        [self.assertIsNone(i) for i in (x, y)]


class DetectDirectionTestCase(TestCase):
    def test_north(self):
        self.assertEquals('n', detect_direction(0, 1, 0, 0))

    def test_south(self):
        self.assertEquals('s', detect_direction(0, 0, 0, 1))

    def test_east(self):
        self.assertEquals('e', detect_direction(0, 0, 1, 0))

    def test_west(self):
        self.assertEquals('w', detect_direction(1, 0, 0, 0))

    def test_northeast(self):
        self.assertEquals('ne', detect_direction(0, 1, 1, 0))

    def test_northwest(self):
        self.assertEquals('nw', detect_direction(1, 1, 0, 0))

    def test_southwest(self):
        self.assertEquals('sw', detect_direction(1, 0, 0, 1))

    def test_southeast(self):
        self.assertEquals('se', detect_direction(0, 0, 1, 1))

    def test_no_vector(self):
        self.assertIsNone(detect_direction(0, 0, 0, 0))
