from django.conf.urls import include, url
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.routers import DefaultRouter
from rest_framework.test import APITestCase

from api.factories import HexGridFactory
from api.models import HexGrid
from api.views import HexGridViewSet

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'grids', HexGridViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = [
    url(r'^', include(router.urls)),
]


class TestAPIUtils:
    """
    Utility Methods used for testing
    """
    def client(self):
        """
        Subclass should implement self.client or subclass a module like
        APITestCase.

        :return: None
        """
        raise NotImplementedError()

    def create_and_auth_superuser(self):
        """
        Create and auth a superuser.
        :return: None
        """
        User.objects.create_superuser(username='user', email='blah',
                                      password='blah')
        user = User.objects.get(username='user')
        self.client.force_authenticate(user=user)


class TestHexGrid(APITestCase, TestAPIUtils):
    urls = 'api.test_views'

    # Sample data in x-www-form-urlencoded format
    CREATE_GRID_DATA = {
        'data': '000,fff,24b,11d',
        'rows': 2,
        'cols': 2,
    }
    UPDATE_GRID_DATA = {
        'data': '000,1ff,24b,11d',
        'rows': 2,
        'cols': 2,
    }

    def setUp(self):
        self.grid = HexGridFactory()
        self.url_list = reverse('hexgrid-list')
        self.url_detail = reverse('hexgrid-detail', kwargs={
            'pk': self.grid.id
        })
        self.create_and_auth_superuser()

    def test_create(self):
        response = self.client.post(self.url_list, self.CREATE_GRID_DATA,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        grid = HexGrid.objects.get(id=response.data['id'])
        self.assertIsNotNone(grid)

        # try to create the same grid
        response = self.client.post(self.url_list, self.CREATE_GRID_DATA,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_bad_rows(self):
        bad_data = self.CREATE_GRID_DATA.copy()
        for rows in ('-2', None, '', 0, 3):
            bad_data.update({'rows': rows})
            response = self.client.post(self.url_list, bad_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_bad_data(self):
        bad_data = self.CREATE_GRID_DATA.copy()
        for data in ('', None, 0, 'z,00f,123,456'):
            bad_data.update({'data': data})
            response = self.client.post(self.url_list, bad_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_list(self):
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve(self):
        response = self.client.get(self.url_detail)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update(self):
        response = self.client.put(self.url_detail, self.UPDATE_GRID_DATA)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # make sure data changed
        self.assertEqual(response.data['data'], self.UPDATE_GRID_DATA['data'])
