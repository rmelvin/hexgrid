'''
Assumptions:
1) robot can only see adjacent grid positions in front of him and not all
   grid positions, so solution is potentially sub-optimal since robot is not
   "all knowing" of all grid position values.
2) grid contains only valid hexadecimal input
3) "least cost path" means the path with smallest gradient
   e.g. if current position is 000, right is FFF, down is 999, then the
   least cost path is from 000 -> 999 (since 999 - 000 < FFF - 000)
'''
# TODO: test on rectangle grid data structure
from operator import itemgetter


def load_grid_from_seq(data, rows, cols):
    if rows * cols != len(data):
        raise Exception('data, rows, and cols mismatch')

    row_range = range(rows)
    grid = [[] for i in row_range]
    for y in range(rows):
        for x in range(cols):
            grid[y].append(data.pop(0))

    return grid


def main():
    NUM_ROWS = 6
    NUM_COLS = 6
    START_X = 0
    START_Y = 0
    END_X = NUM_COLS - 1
    END_Y = NUM_ROWS - 1
    data = ['46B', 'E59', 'EA', 'C1F', '45E', '63', '899', 'FFF', '926',
            '7AD', 'C4E', 'FFF', 'E2E', '323', '6D2', '976', '83F', 'C96',
            '9E9', 'A8B', '9C1', '461', 'F74', 'D05', 'EDD', 'E94', '5F4',
            'D1D', 'D03', 'DE3', '89', '925', 'CF9', 'CA0', 'F18', '4D2']
    grid = load_grid_from_seq(data, NUM_ROWS, NUM_COLS)
    points = [(START_X, START_Y)]
    next_x, next_y = find_next_se(grid, START_X, START_Y, end_x=END_X,
                               end_y=END_Y)
    while (next_x, next_y) != (END_X, END_Y):
        points.append((next_x, next_y))
        next_x, next_y = find_next_se(grid, next_x, next_y, end_x=END_X,
                                   end_y=END_Y)

    points.append((END_X, END_Y))
    print(points)


def find_next_se(grid, curr_x, curr_y, end_x, end_y):
    curr = grid[curr_y][curr_x]

    if (curr_x == end_x) and (curr_y == end_y):
        return (end_x, end_y)

    if curr_x == end_x:
        choices = {'down': {'x': curr_x, 'y': curr_y + 1, }, }
    elif curr_y == end_y:
        choices = {'right': {'x': curr_x + 1, 'y': curr_y, }, }
    else:
        choices = {'right': {'x': curr_x + 1, 'y': curr_y, },
                   'down': {'x': curr_x, 'y': curr_y + 1, },
                   }
    samples = [(compute_hex_diff(grid[position['y']][position['x']], curr),
                {'x': position['x'], 'y': position['y'], }
                )
               for (direction, position) in choices.items()
               ]
    pick = min(samples, key=itemgetter(0))
    return (pick[1].get('x'), pick[1].get('y'))


def compute_hex_diff(hex1, hex2):
    return int(hex1, base=16) - int(hex2, base=16)


if __name__ == '__main__':
    main()
